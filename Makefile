.PHONY: install fmt lint lint-code lint-commits test changelog commit-changes create-tag push-changes watch release

all: install fmt verify

install:
	go get -u github.com/golang/dep/cmd/dep
	go get -u github.com/githubnemo/CompileDaemon
	go get -u golang.org/x/tools/cmd/goimports
	go get -u gopkg.in/alecthomas/gometalinter.v2
	go get github.com/snikch/goodman/cmd/goodman
	npm install
	dep ensure
	gometalinter.v2 --install

fmt:
	go fmt ./...
	goimports -w $$(find . -type f -name '*.go' -not -path "./vendor/*")

lint: lint-code lint-commits

lint-code:
	gometalinter.v2 -D gotypex -D gotype -D vet --vendor --exclude ".*should have comment or be unexported.*" ./...

lint-commits:
	npm run lint-commits

test:
	go test ./...

verify: fmt lint test

changelog:
	npm run generate-changelog

commit-changes:
	git add -A
	git commit -m"chore: release ${VERSION}"

create-tag:
	git tag ${VERSION}

push-changes:
	git push -u origin master --tags

watch:
	CompileDaemon -color=true -exclude-dir=.git -exclude-dir=node_modules -exclude-dir=listeners/pact/pacts -build="make test"

release: verify changelog commit-changes create-tag push-changes
