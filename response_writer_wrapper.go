package recorder

import (
	"bufio"
	"net"
	"net/http"
)

type WrappedResponseWriter interface {
	http.ResponseWriter
	StatusCode() int
	Bytes() []byte
}

func WrapResponseWriter(writer http.ResponseWriter) WrappedResponseWriter {
	_, isCloseNotifier := writer.(http.CloseNotifier)
	_, isFlusher := writer.(http.Flusher)
	_, isHijacker := writer.(http.Hijacker)

	defaultWriter := basicWriter{ResponseWriter: writer}

	if isCloseNotifier && isHijacker && isFlusher {
		return &fancyWriter{&defaultWriter}
	}

	if isFlusher {
		return &flushWriter{&defaultWriter}
	}

	return &defaultWriter

}

type basicWriter struct {
	http.ResponseWriter
	code        int
	wroteHeader bool
	bytes       []byte
}

func (writer *basicWriter) StatusCode() int {
	return writer.code
}

func (writer *basicWriter) Bytes() []byte {
	return writer.bytes
}

func (writer *basicWriter) WriteHeader(status int) {
	if !writer.wroteHeader {
		writer.code = status
		writer.wroteHeader = true
		writer.ResponseWriter.WriteHeader(status)
	}
}

func (writer *basicWriter) Write(bytes []byte) (int, error) {
	writer.bytes = bytes
	writer.WriteHeader(http.StatusOK)
	return writer.ResponseWriter.Write(bytes)
}

type fancyWriter struct {
	*basicWriter
}

func (f *fancyWriter) CloseNotify() <-chan bool {
	closeNotifier := f.basicWriter.ResponseWriter.(http.CloseNotifier)
	return closeNotifier.CloseNotify()
}

func (f *fancyWriter) Flush() {
	fl := f.basicWriter.ResponseWriter.(http.Flusher)
	fl.Flush()
}

func (f *fancyWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	hj := f.basicWriter.ResponseWriter.(http.Hijacker)
	return hj.Hijack()
}

type flushWriter struct {
	*basicWriter
}

func (f *flushWriter) Flush() {
	fl := f.basicWriter.ResponseWriter.(http.Flusher)
	fl.Flush()
}
