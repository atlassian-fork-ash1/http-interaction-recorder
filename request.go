package recorder

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
)

type Request struct {
	Header   http.Header
	Method   string
	Path     string
	Fragment string
	Query    string
	Body     io.Reader
}

func createRequest(r *http.Request) Request {
	body, err := readBody(r.Body)
	if err != nil {
		panic(err)
	}
	if body != nil {
		r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	}

	return Request{
		Header:   copyValues(r.Header),
		Method:   r.Method,
		Path:     getRawPath(r.URL),
		Fragment: r.URL.Fragment,
		Query:    r.URL.RawQuery,
		Body:     bytes.NewBuffer(body),
	}
}

func getRawPath(url *url.URL) string {
	if len(url.RawPath) > 0 {
		return url.RawPath
	}

	return url.Path
}

func readBody(requestBody io.Reader) ([]byte, error) {
	if requestBody == nil {
		return nil, nil
	}

	body, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return nil, err
	}

	return body, nil
}
