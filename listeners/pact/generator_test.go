package pact_test

import (
	"errors"
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/atlassian/http-interaction-recorder"
	"bitbucket.org/atlassian/http-interaction-recorder/listeners/pact"
	"bitbucket.org/atlassian/http-interaction-recorder/mocks"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSavePactFile(t *testing.T) {
	t.Run("it should create default 'pacts' folder, if it does not exist", func(t *testing.T) {
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathDoesNotExist()
		mockFileSystem.GivenCreatePathSucceeds()
		mockFileSystem.GivenOpenWriteStreamSucceedsWith(mocks.NewMockWriteCloser())

		newPactFileGeneratorInvocation(fileSystem(mockFileSystem)).invokeProcess()

		mockFileSystem.AssertCalled(t, "CreatePath", "./pacts")
	})

	t.Run("it should return an error, if checking the path fails", func(t *testing.T) {
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathExistsFailsWith(errors.New("PathExists"))

		err := newPactFileGeneratorInvocation(fileSystem(mockFileSystem)).invokeProcess()

		require.EqualError(t, err, "failed to check path './pacts': PathExists")
	})

	t.Run("it should return an error, if creating path fails", func(t *testing.T) {
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathDoesNotExist()
		mockFileSystem.GivenCreatePathFailsWith(errors.New("CreatePath"))

		err := newPactFileGeneratorInvocation(fileSystem(mockFileSystem)).invokeProcess()

		require.EqualError(t, err, "failed to create path './pacts': CreatePath")
	})

	t.Run("it should save pact file with correct name and unique id", func(t *testing.T) {
		mockIDGenerator := mocks.NewMockIDGenerator()
		mockIDGenerator.GivenGeneratorReturnsID("123")

		invocation := newPactFileGeneratorInvocation(consumer("consumer"), provider("provider"), idGenerator(mockIDGenerator))
		invocation.invokeProcess()

		require.Equal(t, "pacts/consumer-provider-123.json", invocation.getPactLocation())
	})

	t.Run("it should normalize consumer and provider names in the path", func(t *testing.T) {
		mockIDGenerator := mocks.NewMockIDGenerator()
		mockIDGenerator.GivenGeneratorReturnsID("123")

		invocation := newPactFileGeneratorInvocation(consumer("el.Cons_umidor./Más.Importante☃"), provider("☃proveedorEspañol?*/"), idGenerator(mockIDGenerator))
		invocation.invokeProcess()

		require.Equal(t, "pacts/elCons_umidorMasImportante-proveedorEspanol-123.json", invocation.getPactLocation())
	})

	t.Run("it should use the same unique id for multiple invocation on the same generator", func(t *testing.T) {
		mockIDGenerator := mocks.NewMockIDGenerator()
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathExists("./pacts")
		mockFileSystem.GivenOpenWriteStreamSucceedsWith(mocks.NewMockWriteCloser())

		mockIDGenerator.GivenGeneratorReturnsID("123")
		pactGenerator := pact.NewGenerator(
			"consumer",
			"provider",
			pact.WithFileSystem(mockFileSystem),
			pact.WithIDGenerator(mockIDGenerator),
		)

		pactGenerator.ProcessInteraction(recorder.Request{}, recorder.Response{})
		firstLocation := mockFileSystem.GetPactLocation()

		pactGenerator.ProcessInteraction(recorder.Request{}, recorder.Response{})
		secondLocation := mockFileSystem.GetPactLocation()

		require.Equal(t, firstLocation, secondLocation)
		mockIDGenerator.AssertNumberOfCalls(t, "Generate", 1)
	})

	t.Run("it should use the same pact for multiple invocation on the same generator", func(t *testing.T) {
		mockIDGenerator := mocks.NewMockIDGenerator()
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathExists("./pacts")
		mockWriter := mocks.NewMockWriteCloser()
		mockFileSystem.GivenOpenWriteStreamSucceedsWith(mockWriter)

		mockIDGenerator.GivenGeneratorReturnsID("123")
		pactGenerator := pact.NewGenerator(
			"consumer",
			"provider",
			pact.WithFileSystem(mockFileSystem),
			pact.WithIDGenerator(mockIDGenerator),
		)

		pactGenerator.ProcessInteraction(recorder.Request{Method: "GET"}, recorder.Response{StatusCode: 200})
		pactGenerator.ProcessInteraction(recorder.Request{Method: "POST"}, recorder.Response{StatusCode: 201})

		assert.Equal(t, len(mockWriter.GetWrittenPact().Interactions), 2, "interactions")
	})

	t.Run("it should generate a new file name for every generator instance", func(t *testing.T) {
		mockIDGenerator := mocks.NewMockIDGenerator()

		mockIDGenerator.GivenGeneratorReturnsID("123")
		invocation := newPactFileGeneratorInvocation(consumer("consumer"), provider("provider-one"), idGenerator(mockIDGenerator))
		invocation.invokeProcess()

		require.Equal(t, "pacts/consumer-provider-one-123.json", invocation.getPactLocation())

		mockIDGenerator.Reset()
		mockIDGenerator.GivenGeneratorReturnsID("345")
		invocation = newPactFileGeneratorInvocation(consumer("consumer"), provider("provider-two"), idGenerator(mockIDGenerator))
		invocation.invokeProcess()

		require.Equal(t, "pacts/consumer-provider-two-345.json", invocation.getPactLocation())
	})

	t.Run("it should return an error, if opening file for saving fails", func(t *testing.T) {
		mockIDGenerator := mocks.NewMockIDGenerator()
		mockIDGenerator.GivenGeneratorReturnsID("123")
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathExists("./pacts")
		mockFileSystem.GivenOpenWriteStreamFailsWith(errors.New("OpenFile"))

		newPactFileGeneratorInvocation(consumer("consumer"), provider("provider-two"), idGenerator(mockIDGenerator))
		err := newPactFileGeneratorInvocation(
			consumer("consumer"),
			provider("provider"),
			idGenerator(mockIDGenerator),
			fileSystem(mockFileSystem),
		).invokeProcess()

		require.EqualError(t, err, "failed to open file for write 'pacts/consumer-provider-123.json': OpenFile")
	})

	t.Run("it should not html escape content", func(t *testing.T) {
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathExists("./pacts")
		writer := mocks.NewMockWriteCloser()
		mockFileSystem.GivenOpenWriteStreamSucceedsWith(writer)

		newPactFileGeneratorInvocation(
			provider("prov<i>der"),
			fileSystem(mockFileSystem),
		).invokeProcess()

		pactContent := writer.GetWrittenContent()
		assert.True(t, strings.Contains(pactContent, `"name": "prov<i>der"`), fmt.Sprintf("'%v' contains provider name", pactContent))
	})

	t.Run("it should pretty print json file", func(t *testing.T) {
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathExists("./pacts")
		writer := mocks.NewMockWriteCloser()
		mockFileSystem.GivenOpenWriteStreamSucceedsWith(writer)

		newPactFileGeneratorInvocation(
			provider("provider"),
			consumer("consumer"),
			fileSystem(mockFileSystem),
		).invokeProcess()

		pactContent := writer.GetWrittenContent()
		assert.True(t, strings.Contains(pactContent, "  \"name\": \"consumer\"\n"), fmt.Sprintf("'%v' is pretty printed", pactContent))
	})

	t.Run("it should close file after saving the content", func(t *testing.T) {
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathExists("./pacts")
		writer := mocks.NewMockWriteCloser()
		mockFileSystem.GivenOpenWriteStreamSucceedsWith(writer)

		newPactFileGeneratorInvocation(
			fileSystem(mockFileSystem),
		).invokeProcess()

		assert.True(t, writer.WasClosed(), "writer was closed")
	})

	t.Run("it should return an error if saving content fails", func(t *testing.T) {
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathExists("./pacts")
		writer := mocks.NewMockWriteCloser()
		writer.GivenWriteFailsWith(errors.New("WriteContent"))
		mockFileSystem.GivenOpenWriteStreamSucceedsWith(writer)

		err := newPactFileGeneratorInvocation(
			fileSystem(mockFileSystem),
		).invokeProcess()

		require.EqualError(t, err, "failed to save files content: WriteContent")
	})

	t.Run("it should return an error if closing file fails", func(t *testing.T) {
		mockFileSystem := mocks.NewMockFileSystem()
		mockFileSystem.GivenPathExists("./pacts")
		writer := mocks.NewMockWriteCloser()
		writer.GivenCloseFailsWith(errors.New("CloseFile"))
		mockFileSystem.GivenOpenWriteStreamSucceedsWith(writer)

		err := newPactFileGeneratorInvocation(
			fileSystem(mockFileSystem),
		).invokeProcess()

		require.EqualError(t, err, "failed to close file: CloseFile")
	})
}
