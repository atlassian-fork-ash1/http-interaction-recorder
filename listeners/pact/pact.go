package pact

import "encoding/json"

type Request struct {
	Method  string            `json:"method"`
	Path    string            `json:"path"`
	Query   string            `json:"query,omitempty"`
	Headers map[string]string `json:"headers,omitempty"`
	Body    json.RawMessage   `json:"body,omitempty"`
}

type Response struct {
	Status  int               `json:"status"`
	Headers map[string]string `json:"headers,omitempty"`
	Body    json.RawMessage   `json:"body,omitempty"`
}

type Interaction struct {
	Request     Request  `json:"request"`
	Response    Response `json:"response"`
	Description string   `json:"description"`
}

type Pact struct {
	Consumer     Participant   `json:"consumer"`
	Provider     Participant   `json:"provider"`
	Interactions []Interaction `json:"interactions"`
}

type Participant struct {
	Name string `json:"name"`
}

func (pact *Pact) AddInteraction(interaction Interaction) {
	interactions := append(pact.Interactions, interaction)
	pact.Interactions = interactions
}
