package pact

import (
	"bitbucket.org/atlassian/http-interaction-recorder"
)

const folderName = "./pacts"

type Generator struct {
	fileSaver *fileSaver
	pact      Pact
}

type config struct {
	fileSystem  FileSystem
	idGenerator IDGenerator
}

func (generator *Generator) ProcessInteraction(request recorder.Request, response recorder.Response) error {
	generator.pact.AddInteraction(toInteraction(request, response))
	return generator.fileSaver.savePactFile(generator.pact, folderName)
}

func (generator *Generator) getPactLocation() string {
	return generator.fileSaver.getPactFileName(folderName, generator.pact)
}

func WithFileSystem(fileSystem FileSystem) Option {
	return func(config *config) {
		config.fileSystem = fileSystem
	}
}

func WithIDGenerator(idGenerator IDGenerator) Option {
	return func(config *config) {
		config.idGenerator = idGenerator
	}
}

type Option func(config *config)

func NewGenerator(consumer, provider string, options ...Option) *Generator {
	consumer = notNullNorBlank(consumer, "consumer name")
	provider = notNullNorBlank(provider, "provider name")
	config := createConfig(options...)
	fileSaver := newFileSaver(config.fileSystem, config.idGenerator)
	pact := Pact{Consumer: Participant{Name: consumer}, Provider: Participant{Name: provider}}

	return &Generator{
		pact:      pact,
		fileSaver: fileSaver,
	}
}

func createConfig(options ...Option) *config {
	config := &config{
		fileSystem:  &LocalFileSystem{},
		idGenerator: newIDGenerator(),
	}
	for _, option := range options {
		option(config)
	}
	return config
}
