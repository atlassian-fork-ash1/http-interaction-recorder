package pact

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"path/filepath"
	"regexp"

	"golang.org/x/text/unicode/norm"
)

var illegalName = regexp.MustCompile(`[^[:alnum:]-_]`)

type fileSaver struct {
	fileSystem FileSystem
	uniqueID   string
}

func (saver *fileSaver) savePactFile(pact Pact, folderName string) (err error) {
	err = saver.ensureFolderExists(folderName)
	if err != nil {
		return err
	}

	fileName := saver.getPactFileName(folderName, pact)
	file, err := saver.fileSystem.OpenWriteStream(fileName)
	if err != nil {
		return fmt.Errorf("failed to open file for write '%v': %v", fileName, err.Error())
	}
	defer func(closer io.WriteCloser) {
		closeErr := closer.Close()
		if err == nil && closeErr != nil {
			err = fmt.Errorf("failed to close file: %v", closeErr.Error())
		}
	}(file)

	err = savePactContent(file, pact)
	return
}

func (saver *fileSaver) getPactFileName(folder string, pact Pact) string {
	consumer := sanitizeName(pact.Consumer.Name)
	provider := sanitizeName(pact.Provider.Name)
	return filepath.Join(folder, fmt.Sprintf("%v-%v-%v.json", consumer, provider, saver.uniqueID))
}

func (saver *fileSaver) ensureFolderExists(folderPath string) error {
	pactsLocationExists, err := saver.fileSystem.PathExists(folderPath)
	if err != nil {
		return fmt.Errorf("failed to check path '%v': %v", folderPath, err.Error())
	}

	if !pactsLocationExists {
		err = saver.fileSystem.CreatePath(folderPath)
		if err != nil {
			return fmt.Errorf("failed to create path '%v': %v", folderPath, err.Error())
		}
	}
	return nil
}

func savePactContent(file io.Writer, pact Pact) error {
	writer := bufio.NewWriter(file)
	encoder := json.NewEncoder(writer)
	encoder.SetEscapeHTML(false)
	encoder.SetIndent("", "  ")
	err := encoder.Encode(pact)
	if err != nil {
		return err
	}

	err = writer.Flush()
	if err != nil {
		return fmt.Errorf("failed to save files content: %v", err.Error())
	}

	return err
}

func sanitizeName(name string) string {
	name = norm.NFD.String(name)
	return illegalName.ReplaceAllString(name, "")
}

func newFileSaver(fileSystem FileSystem, generator IDGenerator) *fileSaver {
	return &fileSaver{
		fileSystem: fileSystem,
		uniqueID:   generator.Generate(),
	}
}
