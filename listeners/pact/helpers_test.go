package pact_test

import (
	"encoding/json"

	"bitbucket.org/atlassian/http-interaction-recorder"
	"bitbucket.org/atlassian/http-interaction-recorder/listeners/pact"
	"bitbucket.org/atlassian/http-interaction-recorder/mocks"
)

type pactFileGeneratorInvocation struct {
	consumer    string
	provider    string
	fileSystem  pact.FileSystem
	idGenerator pact.IDGenerator
	request     recorder.Request
	response    recorder.Response
}

func (invocation *pactFileGeneratorInvocation) invokeProcess() error {
	pactGenerator := pact.NewGenerator(
		invocation.consumer,
		invocation.provider,
		pact.WithFileSystem(invocation.fileSystem),
		pact.WithIDGenerator(invocation.idGenerator),
	)
	return pactGenerator.ProcessInteraction(invocation.request, invocation.response)
}

func (invocation *pactFileGeneratorInvocation) getPactLocation() string {
	return invocation.fileSystem.(*mocks.MockFileSystem).GetPactLocation()
}

func (invocation *pactFileGeneratorInvocation) getPactAsJson() jsonPact {
	mockWriteCloser := invocation.fileSystem.(*mocks.MockFileSystem).GetWriter().(*mocks.MockWriteCloser)
	return jsonPact{content: mockWriteCloser.GetWrittenContent()}
}

type generatorOption func(invocation *pactFileGeneratorInvocation)

func consumer(consumer string) generatorOption {
	return func(invocation *pactFileGeneratorInvocation) {
		invocation.consumer = consumer
	}
}

func provider(provider string) generatorOption {
	return func(invocation *pactFileGeneratorInvocation) {
		invocation.provider = provider
	}
}

func fileSystem(fileSystem pact.FileSystem) generatorOption {
	return func(invocation *pactFileGeneratorInvocation) {
		invocation.fileSystem = fileSystem
	}
}

func idGenerator(idGenerator pact.IDGenerator) generatorOption {
	return func(invocation *pactFileGeneratorInvocation) {
		invocation.idGenerator = idGenerator
	}
}

func request(request recorder.Request) generatorOption {
	return func(invocation *pactFileGeneratorInvocation) {
		invocation.request = request
	}
}

func response(response recorder.Response) generatorOption {
	return func(invocation *pactFileGeneratorInvocation) {
		invocation.response = response
	}
}

func newPactFileGeneratorInvocation(options ...generatorOption) *pactFileGeneratorInvocation {
	mockFileSystem := mocks.NewMockFileSystem()
	mockFileSystem.GivenPathExists("./pacts")
	mockFileSystem.GivenOpenWriteStreamSucceedsWith(mocks.NewMockWriteCloser())

	mockIDGenerator := mocks.NewMockIDGenerator()
	mockIDGenerator.GivenGeneratorReturnsID("default-uuid")

	invocation := &pactFileGeneratorInvocation{
		consumer:    "default-consumer",
		provider:    "default-provider",
		fileSystem:  mockFileSystem,
		idGenerator: mockIDGenerator,
		request:     recorder.Request{},
		response: recorder.Response{
			StatusCode: 200,
		},
	}

	for _, option := range options {
		option(invocation)
	}

	return invocation
}

type jsonPact struct {
	content string
}

func (pact *jsonPact) getConsumer() string {
	consumer := pactAsJsonObject([]byte(pact.content))["consumer"].(map[string]interface{})
	return consumer["name"].(string)
}

func (pact *jsonPact) getProvider() string {
	provider := pactAsJsonObject([]byte(pact.content))["provider"].(map[string]interface{})
	return provider["name"].(string)
}

func (pact *jsonPact) getFirstRequestMethod() string {
	firstInteractionRequest := pact.getFirstInteractionRequest()
	return firstInteractionRequest["method"].(string)
}

func (pact *jsonPact) getFirstRequestPath() string {
	firstInteractionRequest := pact.getFirstInteractionRequest()
	return firstInteractionRequest["path"].(string)
}

func (pact *jsonPact) getFirstRequestQuery() string {
	firstInteractionRequest := pact.getFirstInteractionRequest()
	return firstInteractionRequest["query"].(string)
}

func (pact *jsonPact) getFirstRequestHeaders() map[string]interface{} {
	firstInteractionRequest := pact.getFirstInteractionRequest()
	return firstInteractionRequest["headers"].(map[string]interface{})
}

func (pact *jsonPact) getFirstRequestBody() string {
	firstInteractionRequest := pact.getFirstInteractionRequest()
	return firstInteractionRequest["body"].(string)
}

func (pact *jsonPact) getFirstRequestRawBody() interface{} {
	firstInteractionRequest := pact.getFirstInteractionRequest()
	return firstInteractionRequest["body"]
}

func (pact *jsonPact) getFirstResponseRawBody() interface{} {
	firstInteractionRequest := pact.getFirstInteractionResponse()
	return firstInteractionRequest["body"]
}

func (pact *jsonPact) getFirstResponseStatusCode() int {
	firstInteractionResponse := pact.getFirstInteractionResponse()
	status := firstInteractionResponse["status"].(float64)
	return int(status)
}

func (pact *jsonPact) getFirstResponseHeaders() map[string]interface{} {
	firstInteractionResponse := pact.getFirstInteractionResponse()
	return firstInteractionResponse["headers"].(map[string]interface{})
}

func (pact *jsonPact) getFirstResponseBody() string {
	firstInteractionResponse := pact.getFirstInteractionResponse()
	return firstInteractionResponse["body"].(string)
}

func (pact *jsonPact) getFirstInteraction() map[string]interface{} {
	interactions := pactAsJsonObject([]byte(pact.content))["interactions"].([]interface{})
	return interactions[0].(map[string]interface{})
}

func (pact *jsonPact) getFirstInteractionRequest() map[string]interface{} {
	firstInteraction := pact.getFirstInteraction()
	return firstInteraction["request"].(map[string]interface{})
}

func (pact *jsonPact) getFirstInteractionResponse() map[string]interface{} {
	firstInteraction := pact.getFirstInteraction()
	return firstInteraction["response"].(map[string]interface{})
}

func (pact *jsonPact) getFirstInteractionDescription() string {
	firstInteraction := pact.getFirstInteraction()
	return firstInteraction["description"].(string)
}

func pactAsJsonObject(content []byte) map[string]interface{} {
	var jsonObject map[string]interface{}
	json.Unmarshal([]byte(content), &jsonObject)
	return jsonObject
}
