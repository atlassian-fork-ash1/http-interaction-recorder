package pact

import (
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"encoding/json"
	"fmt"

	"bitbucket.org/atlassian/http-interaction-recorder"
)

func toInteraction(request recorder.Request, response recorder.Response) Interaction {
	return Interaction{
		Request:     toPactRequest(request),
		Response:    toPactResponse(response),
		Description: getInteractionDescription(request, response),
	}
}

func getInteractionDescription(request recorder.Request, response recorder.Response) string {
	return fmt.Sprintf("%v %v -> %v", request.Method, request.Path, response.StatusCode)
}

func toPactResponse(response recorder.Response) Response {
	return Response{
		Status:  response.StatusCode,
		Headers: getHeaders(response.Header),
		Body:    readBody(response.Body),
	}
}

func toPactRequest(request recorder.Request) Request {
	return Request{
		Method:  request.Method,
		Path:    request.Path,
		Query:   request.Query,
		Headers: getHeaders(request.Header),
		Body:    readBody(request.Body),
	}
}
func readBody(body io.Reader) json.RawMessage {
	if body != nil {
		content, _ := ioutil.ReadAll(body)
		if len(content) > 0 {
			return json.RawMessage(content)
		}
	}

	return nil
}

func getHeaders(header http.Header) map[string]string {
	var headers = make(map[string]string, len(header))
	for headerName, headerValues := range header {
		headers[strings.ToLower(headerName)] = strings.Join(headerValues, ", ")
	}

	return headers
}
