package pact

import (
	"fmt"
	"sync"

	"bitbucket.org/atlassian/http-interaction-recorder"
)

type generatorRegistry struct {
	generators map[string]*Generator
	lock       sync.Mutex
}

var (
	r    *generatorRegistry
	once sync.Once
)

func registry() *generatorRegistry {
	once.Do(func() {
		r = &generatorRegistry{
			generators: make(map[string]*Generator),
			lock:       sync.Mutex{},
		}
	})

	return r
}

type interaction struct {
	request  recorder.Request
	response recorder.Response
}

func (registry *generatorRegistry) processInteraction(consumer, provider string, interaction interaction) error {
	registry.lock.Lock()
	defer registry.lock.Unlock()

	generator := registry.getGenerator(consumer, provider)
	return generator.ProcessInteraction(interaction.request, interaction.response)
}

func (registry *generatorRegistry) getGenerator(consumer, provider string) *Generator {
	generatorName := getGeneratorName(consumer, provider)
	generator, found := registry.generators[generatorName]
	if !found {
		generator = NewGenerator(consumer, provider)
		registry.generators[generatorName] = generator
	}

	return generator
}

func (registry *generatorRegistry) getPactLocation(consumer, provider string) string {
	generatorName := getGeneratorName(consumer, provider)
	generator, found := registry.generators[generatorName]
	if !found {
		return ""
	}

	return generator.getPactLocation()
}

func getGeneratorName(consumer, provider string) string {
	return fmt.Sprintf("%v-%v", consumer, provider)
}
