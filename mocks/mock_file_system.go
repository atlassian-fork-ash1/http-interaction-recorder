package mocks

import (
	"io"

	"github.com/stretchr/testify/mock"
)

type MockFileSystem struct {
	mock.Mock
	pactLocation string
	writer       io.WriteCloser
}

func (fileSystem *MockFileSystem) PathExists(path string) (bool, error) {
	args := fileSystem.Called(path)
	return args.Bool(0), args.Error(1)
}

func (fileSystem *MockFileSystem) CreatePath(path string) error {
	args := fileSystem.Called(path)
	return args.Error(0)
}

func (fileSystem *MockFileSystem) OpenWriteStream(filePath string) (io.WriteCloser, error) {
	args := fileSystem.Called(filePath)
	fileSystem.pactLocation = filePath
	writeCloser, _ := args.Get(0).(io.WriteCloser)
	return writeCloser, args.Error(1)
}

func (fileSystem *MockFileSystem) GetPactLocation() string {
	return fileSystem.pactLocation
}

func (fileSystem *MockFileSystem) GetWriter() io.WriteCloser {
	return fileSystem.writer
}

func (fileSystem *MockFileSystem) GivenPathDoesNotExist() {
	fileSystem.On("PathExists", mock.Anything).Return(false, nil)
}

func (fileSystem *MockFileSystem) GivenPathExistsFailsWith(err error) {
	fileSystem.On("PathExists", mock.Anything).Return(false, err)
}

func (fileSystem *MockFileSystem) GivenPathExists(path string) {
	fileSystem.On("PathExists", path).Return(true, nil)
}

func (fileSystem *MockFileSystem) GivenCreatePathFailsWith(err error) {
	fileSystem.On("CreatePath", mock.Anything).Return(err)
}

func (fileSystem *MockFileSystem) GivenCreatePathSucceeds() {
	fileSystem.On("CreatePath", mock.Anything).Return(nil)
}

func (fileSystem *MockFileSystem) GivenOpenWriteStreamFailsWith(err error) {
	fileSystem.On("OpenWriteStream", mock.Anything).Return(nil, err)
}

func (fileSystem *MockFileSystem) GivenOpenWriteStreamSucceedsWith(writeCloser io.WriteCloser) {
	fileSystem.writer = writeCloser
	fileSystem.On("OpenWriteStream", mock.Anything).Return(writeCloser, nil)
}

func NewMockFileSystem() *MockFileSystem {
	mockFileSystem := new(MockFileSystem)
	return mockFileSystem
}
