package mocks

import (
	"sync"

	recorder "bitbucket.org/atlassian/http-interaction-recorder"
)

type MockListener struct {
	request       recorder.Request
	response      recorder.Response
	calledCounter int
	lock          sync.Mutex
}

func (listener *MockListener) RequestReceived(request recorder.Request, response recorder.Response) {
	listener.lock.Lock()
	defer listener.lock.Unlock()
	listener.response = response
	listener.request = request
	listener.calledCounter = listener.calledCounter + 1
}

func (listener *MockListener) GetRequest() recorder.Request {
	return listener.request
}

func (listener *MockListener) GetResponse() recorder.Response {
	return listener.response
}

func (listener *MockListener) CalledTimes() int {
	return listener.calledCounter
}

func NewMockListener() *MockListener {
	return &MockListener{lock: sync.Mutex{}}
}
