package mocks

import (
	"bufio"
	"net"
	"net/http"
)

type MockCloseNotifierResponseWriter struct {
	http.ResponseWriter
	CalledCloseNotify bool
}

func (r *MockCloseNotifierResponseWriter) CloseNotify() <-chan bool {
	r.CalledCloseNotify = true
	return make(<-chan bool)
}

type MockFlusherResponseWriter struct {
	http.ResponseWriter
	CalledFlush bool
}

func (r *MockFlusherResponseWriter) Flush() {
	r.CalledFlush = true
}

type MockHijackerResponseWriter struct {
	http.ResponseWriter
	CalledHijack bool
}

func (r *MockHijackerResponseWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	r.CalledHijack = true
	return nil, nil, nil
}

type MockBasicResponseWriter struct {
	Code int
}

func (basicWriter *MockBasicResponseWriter) Write(bytes []byte) (int, error) {
	return len(bytes), nil
}

func (basicWriter *MockBasicResponseWriter) WriteHeader(code int) {
	basicWriter.Code = code
}

func (basicWriter *MockBasicResponseWriter) Header() http.Header {
	return make(http.Header)
}

type MockResponseWriter struct {
	MockBasicResponseWriter
	MockHijackerResponseWriter
	MockFlusherResponseWriter
	MockCloseNotifierResponseWriter
}
