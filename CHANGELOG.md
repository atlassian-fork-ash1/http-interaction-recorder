<a name="0.0.5"></a>
## [0.0.5](https://bitbucket.org/atlassian/httptest-interaction-listener/compare/v0.0.4...v0.0.5) (2018-02-19)


### Bug Fixes

* format of generated pact file and related tests ([46db1ce](https://bitbucket.org/atlassian/httptest-interaction-listener/commits/46db1ce))



<a name="0.0.4"></a>
## [0.0.4](https://bitbucket.org/atlassian/httptest-interaction-listener/compare/v0.0.3...v0.0.4) (2018-02-19)


### Bug Fixes

* fix bb pipelines setup ([8a711a3](https://bitbucket.org/atlassian/httptest-interaction-listener/commits/8a711a3))



<a name="0.0.3"></a>
## [0.0.3](https://bitbucket.org/atlassian/httptest-interaction-listener/compare/v0.0.2...v0.0.3) (2017-12-04)


### Bug Fixes

* fix request path to contain raw path ([cbc005d](https://bitbucket.org/atlassian/httptest-interaction-listener/commits/cbc005d))



<a name="0.0.2"></a>
# [0.0.2](https://bitbucket.org/atlassian/httptest-interaction-listener/compare/v0.0.2...v0.0.1) (2017-12-03)


### Features

* add pact listener to record interactions as pact files ([ee8c75d](https://bitbucket.org/atlassian/http-interaction-recorder/commits/ee8c75d))



<a name="0.0.1"></a>
# [0.0.1](https://bitbucket.org/atlassian/httptest-interaction-listener/compare/v0.0.1) (2017-11-28)


### Features

* add http interaction recorder ([11d1963](https://bitbucket.org/atlassian/http-interaction-recorder/commits/11d1963))



